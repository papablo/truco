#include <stdio.h>
#include "card.h"


/**
 * Odd numbered hands belong to the pie players
 *  - negative round_score means pies won
 * Even numbered hands belong to the hand players
 *  - positive round_score means hands won
 */

int to_play = 0;
int get_next_trick_starter(int trick_result){
    if(trick_result < 0) {
        return 1;
    }
    else if(trick_result > 0){
        return 0;
    }
    else { 
        // TODO: Check for draws 
        return -1; 
    }
}

void update_to_play(int *to_play, int n_hands){
    int v = *to_play;
    *to_play =  (v+1) % n_hands;
}


void update_round_score(int to_play, int outcome, int *round_score){
    if (to_play % 2) { // to_play is odd
        if (outcome > 0){
            *round_score--;
        }
        else if(outcome < 0){
            *round_score++;
        }
    }
    else {
        // round_score > 0
        if (outcome > 0){
            *round_score++;
        }
        else if(outcome < 0){
            *round_score--;
        }
    }
}

void play_hands(hand_t *hands, int n_hands){

    int round_score = 0;
    int n_tricks = 3;

    int to_play = 0;

    // Game on!
    for(int trick_i = 0 ; trick_i < n_tricks;trick_i++){

        card_t player_0_card = hands[to_play][trick_i];
        update_to_play(&to_play, n_hands);

        card_t player_1_card = hands[to_play][trick_i];
        update_to_play(&to_play, n_hands);

        int outcome = cardcmp(player_0_card, player_1_card);

        update_round_score(to_play, outcome, &round_score);

        to_play = get_next_trick_starter(outcome);
    }

    if (round_score > 0){
        printf("Ganó hand:0\n");
    }
    else if (round_score < 0){
        printf("Ganó hand:1\n");
    }
    else {
        printf("Draw\n");
    }
}
