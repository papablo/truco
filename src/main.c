#include <stdio.h>
#include "card.h"
#include "game.h"

deck_t deck;
int n_hands = 2;

int main()
{

    initialize_deck(deck);
    printf("\n\n Here we goooo \n");
    shuffle_deck(deck);
    hand_t *hands = deal_hands(deck, n_hands);

    for (int i = 0; i < n_hands; ++i) {
        printf("Mano: %d: \n", i);
        display_hand(hands[i]);
    }

    play_hands(hands, n_hands);
    // Here!
    
    return 0;
}
